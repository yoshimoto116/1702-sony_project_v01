//============================
// TODO:
//   背景のBGM削除
//   途中で入る音声削除
//　　著作権的に曲を入れ替える必要あり
//============================

da.segment.onpreprocess = function (trigger, args) {
    console.log("===y=== : 1-1");
    console.log('onpreprocess', { trigger: trigger, args: args });
    console.log("===y=== : 1-2");
    da.startSegment(null, null);
    console.log("===y=== : 1-3");
};

da.segment.onstart = function (trigger, args) {

    console.log("===y=== : 3-1");

    // ====== 曲のロード ====== //
    //var audio = new Audio('http://www.freesound.org/data/previews/59/59569_571436-lq.mp3'); // 馬の鳴き声
    var audio1 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/9eb563ec0ff2df4f27db5ee1e1479c5924c0b49a/soundA/state_normal.mp3');
    var audio2 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/9eb563ec0ff2df4f27db5ee1e1479c5924c0b49a/soundA/state_denger.mp3');
    var audio3 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/9eb563ec0ff2df4f27db5ee1e1479c5924c0b49a/soundA/state_area.mp3');
    var audio4 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/5d575f4fe4b1df50f64d7c3925d2ffb106bbbd98/soundA/gameover.mp3');
    var audio5 = new Audio('https://bitbucket.org/yuma_yoshimoto/1702-sony_sound_only/raw/5d575f4fe4b1df50f64d7c3925d2ffb106bbbd98/soundA/gameover.mp3');

    // ====== 各種変数初期化 ====== //
    var loop = 0;
    var state = 1; //[1=通常],[2=ゾンビ近い],[3=長時間いるとゾンビ化]
    var latitude=0;
    var longitude=0;
    var walk_or_run = 3;
    var dis = 0;
    var zonbi_counter = 0;
    var human = 1;
    
    userID = "n03";
    zonbiID = "n04";

    audio1.play(); //とりあえず1曲目流し始める
    firebase.database().ref('/root/' + userID+'/human/').set(1);

    var hoge = setInterval(function() {

        // ====== 本番用 : ここから ====== //
            if (dis < 50)
                state=3;
            else if (dis < 100)
                state=2;
            else
                state=1;
        //    state=3;
        // ====== 本番用 : ここまで ====== //
/*
        // ====== デバッグ用 : ここから ====== //
        if (loop == 15)
            state=2;
        else if (loop == 30)
            state=3;
        else if (loop == 45)
        //    state=3;
        // ====== デバッグ用 : ここまで ====== //
*/
        // ====== GPS情報取得 ====== //
        var callbacks = {
            onsuccess: function (result) {
            latitude = result.latitude;
            longitude = result.longitude;
            
            },
            onerror: function (error) {
                console.log('getCurrentPosition fail.' + error.message);
            }
        };
        var option = {
            timeout: 10000,
            enablehighaccuracy: true
        };
        var geo = new da.Geolocation();
        geo.getCurrentPosition(callbacks, option);
        console.log("===y=== : GPS情報 : latitude=",latitude,"_longitude=",longitude);
        firebase.database().ref('/root/' + userID + '/gps' + '/latitude').set(latitude);
        firebase.database().ref('/root/' + userID + '/gps' + '/longitude').set(longitude);


        // ====== 歩いているか、走っているか　ステート ====== //
        var contxtUpdate = function(){
            var cont = new da.ContextEngine();
            var callbackObject ={
                    onsuccess: function (context) {
                        console.log('cntextIs', context.activity);
            cont.getCurrentContext(callbackObject);
                        walk_or_run = context.activity;

            console.log("===y=== : AAAAAAAAAAAAAAAa," + walk_or_run);
        if (walk_or_run == 2)
            firebase.database().ref('/root/' + userID+'/state/').set(0);
        else
            firebase.database().ref('/root/' + userID+'/state/').set(1);
                    }
            }
        }
        /*
        console.log("===y=== : walk or run : walk_or_run=",walk_or_run);
        if (walk_or_run == 2)
            firebase.database().ref('/root/' + userID+'/state/').set(0);
        else
            firebase.database().ref('/root/' + userID+'/state/').set(1);
        */
        // ====== ゾンビデータダウンロード ====== //
        var zonbiID = "n04";
        var zonbi_gps_latitude = 0;
        var zonbi_gps_longitude = 0;
        var zonbi_state = 0;
        firebase.database().ref('/root/'+zonbiID+'/').once('value').then(function(snapshot) {
            //var gps = snapshot.val().gps;
            zonbi_gps_latitude = snapshot.val().gps.latitude;
            console.log("===y=== : 音楽再生 : ぞんびGPS[I]," + zonbi_gps_latitude + "," + zonbi_gps_longitude + "," + zonbi_state);

        });

        firebase.database().ref('/root/'+zonbiID+'/').once('value').then(function(snapshot) {
            zonbi_gps_longitude = snapshot.val().gps.longitude;
            console.log("===y=== : 音楽再生 : ぞんびGPS[II]," + zonbi_gps_latitude + "," + zonbi_gps_longitude + "," + zonbi_state);
        });

        firebase.database().ref('/root/'+zonbiID+'/').once('value').then(function(snapshot) {
            zonbi_state = snapshot.val().state;
            console.log("===y=== : 音楽再生 : ぞんびGPS[III]," + zonbi_gps_latitude + "," + zonbi_gps_longitude + "," + zonbi_state);
        });



        // ====== 音楽再生 ====== //
        console.log("===y=== : 音楽再生 : loop_count=",loop,"_state=",state);
        if (state==1){ // 通常 state=1
            audio1.play();
            audio2.pause();
            audio3.pause();
            zonbi_counter = 0;
        }
        else if (state==2){ // ゾンビ近し state=2
            audio1.pause();
            audio2.play();
            audio3.pause();
            zonbi_counter = 0;
        }
        else if (state==3){ // もうすぐゾンビ state=3
            audio1.pause();
            audio2.pause();
            audio3.play();
            zonbi_counter  = zonbi_counter + 1;
        }

        loop++;

        // ====== 距離計算 ====== //
        dis = distance(latitude,longitude,zonbi_gps_latitude,zonbi_gps_longitude);
        console.log("===y=== : distanse =",dis);

        // ゾンビ化判定
        if (zonbi_counter > 30) {
            audio1.pause();
            audio2.pause();
            audio3.pause();
            audio4.play();
            state=4;
            zonbi_counter  = zonbi_counter + 1;
            firebase.database().ref('/root/' + userID+'/human/').set(0);
        }
        else if (state==4) {
            if(zonbi_counter > 3)
                da.stopSegment();
                console.log("===y=== : ゲームオーバー");
        }

        // 終了条件
        if (loop == 100) {
            audio1.pause();
            audio2.pause();
            audio3.pause();
            audio4.pause();
            audio5.play();
            clearInterval(hoge);
            da.stopSegment();
            console.log("===y=== : ゲームクリア");
        }
        if( loop % 50 ){
                var synthesis = da.SpeechSynthesis.getInstance();
                synthesis.speak('fight!fight!', {
                    onstart: function () {
                        console.log('speak start');
                    },
                    onend: function () {
                        console.log('speak onend');
                    },
                    onerror: function (error) {
                        console.log('speak cancel: ' + error.messsage);
                    }
                });
        }
    }, 1000);

};


function distance(lat1,lon1,lat2,lon2){
    var mode = false;
    var radlat1 = lat1 * Math.PI / 180.0; //経度1
    var radlon1 = lon1 * Math.PI / 180.0; //緯度1
    var radlat2 = lat2 * Math.PI / 180.0; //経度2
    var radlon2 = lon2 * Math.PI / 180.0; //緯度2
    //平均緯度
    var radLatAve = (radlat1 + radlat2) / 2;
    //緯度差
    var radLatDiff = radlat1 - radlat2;
    //経度差算
    var radLonDiff = radlon1 - radlon2;
 
    var sinLat = Math.sin(radLatAve);
    if(mode==true){
	//mode引数がtrueなら世界測地系で計算（デフォルト）
	var tmp =  1.0 - 0.00669438 * (sinLat*sinLat);
	var meridianRad = 6335439.0 / Math.sqrt(tmp*tmp*tmp); // 子午線曲率半径
	var dvrad = 6378137.0 / Math.sqrt(tmp); // 卯酉線曲率半径
    }else{
	//mode引数がfalseなら日本測地系で計算
	var tmp = 1.0 - 0.00667478 * (sinLat*sinLat);
	var meridianRad = 6334834.0 / Math.sqrt(tmp*tmp*tmp); // 子午線曲率半径
	var dvrad = 6377397.155 / Math.sqrt(tmp); // 卯酉線曲率半径
    }
    var t1 = meridianRad * radLatDiff;
    var t2 = dvrad * Math.cos(radLatAve) * radLonDiff;
    var dist = Math.sqrt((t1*t1) + (t2*t2));
 
    dist = Math.floor(dist); //小数点以下切り捨て
    return dist; //２点間の直線距離を返す (単位はm)
}